module gitlab.com/go-bakers/gatewayx

go 1.18

require (
	github.com/aws/aws-lambda-go v1.37.0
	github.com/pkg/errors v0.9.1
)

require github.com/stretchr/testify v1.8.1 // indirect
