package lambda

import (
	"net/http"

	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/go-bakers/gatewayx/aws/event"
	"gitlab.com/go-bakers/gatewayx/aws/gateway"
)

// ListenAndServe drop-in replacement of net/http ListenAndServe.
/* Usage:
r := http.NewServeMux()
r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"status":"ok"}`))
})
lambda.ListenAndServe("", r)
*/
func ListenAndServe(addr string, h http.Handler) error {
	gw, err := gateway.NewGateway(h, gateway.WithEventType(event.EventTypeLambda))
	if err != nil {
		return err
	}
	lambda.Start(gw)
	return nil
}
