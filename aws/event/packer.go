package event

import (
	"context"
	"errors"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
)

// Packer apigateway proxy event packer
type Packer interface {
	NewRequest(ctx context.Context, event events.APIGatewayProxyRequest) (*http.Request, error)
	NewResponse() *ResponseWriter
}

// GetPacker returns Packer implementation
func GetPacker(eventType Type) (Packer, error) {
	switch eventType {
	case EventTypeLambda:
		return NewLambdaPacker(), nil
	}
	return nil, errors.New("invalid Packer")
}
