package gateway

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/go-bakers/gatewayx/aws/event"
)

type Gateway struct {
	h           http.Handler
	eventPacker event.Packer
}

// WithEventType set event type
func WithEventType(eventType event.Type) func(*Gateway) error {
	return func(gw *Gateway) error {
		packer, err := event.GetPacker(eventType)
		if err != nil {
			return err
		}
		gw.eventPacker = packer
		return nil
	}
}

// NewGateway returns a new gateway
func NewGateway(h http.Handler, eventTypeSetter func(*Gateway) error) (*Gateway, error) {
	gw := &Gateway{h: h}
	if err := eventTypeSetter(gw); err != nil {
		return nil, err
	}
	return gw, nil
}

func (me *Gateway) Invoke(ctx context.Context, payload []byte) ([]byte, error) {
	evt := events.APIGatewayProxyRequest{}

	if err := json.Unmarshal(payload, &evt); err != nil {
		return nil, err
	}
	r, err := me.eventPacker.NewRequest(ctx, evt)
	if err != nil {
		return nil, err
	}
	w := me.eventPacker.NewResponse()

	me.h.ServeHTTP(w, r)
	resp := w.End()
	return json.Marshal(&resp)
}
